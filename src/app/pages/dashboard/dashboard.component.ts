import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user: any;

  constructor(private router: Router) { }

  ngOnInit(): void {
    var userData = localStorage.getItem(environment.USER);
    console.log(userData);
    if (userData)
      this.user = JSON.parse(userData);
    else this.router.navigate(['/login']);
  }

}
