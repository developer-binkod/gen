import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiUrl: string = environment.apiUrl;

  constructor(private httpClient: HttpClient, private router: Router) { }

  public login(params: any = {}): Observable<any> {
    return this.httpClient.post<Response>(this.apiUrl + "login", params)
      .pipe(map(res => {
        return res;
      }), catchError(errorResponse => {
        return this.handleError(errorResponse);
      }));
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.log('Client Side Error', errorResponse);
    } else {
      console.log('Server Side Error', errorResponse);
      return throwError(errorResponse.error);
    }
  }

}
